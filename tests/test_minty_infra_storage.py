# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import io
import pytest
from copy import deepcopy
from minio.helpers import ObjectWriteResult
from minty.exceptions import ConfigurationConflict
from minty_infra_storage.s3 import S3Infrastructure, S3Wrapper
from unittest import mock
from uuid import uuid4


class TestS3Infrastructure:
    """Test the S3 infrastructure"""

    def setup_method(self):
        infra = S3Infrastructure()
        self.infra = infra
        self.full_config = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": {
                "type": "s3",
                "name": "S3Local",
                "bucket": "demo-bucket-002",
                "endpoint_url": "http://10.30.2.206:4572",
                "access_id": "test_id",
                "access_key": "test_key",
                "addressing_style": "path",
                "region_name": "xxx",
            },
        }
        general_configs = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3", "name": "S3Local"}],
        }
        self.s3Client = self.infra(general_configs)

    def test_get_infrastructure(self):
        s3 = self.infra(self.full_config)
        assert isinstance(s3, S3Wrapper)

    def test_configuration_exception_s3_infra(self):
        cfg = deepcopy(self.full_config)
        del cfg["storage_bucket"]
        del cfg["instance_uuid"]

        with pytest.raises(
            ConfigurationConflict,
            match="No instance UUID or storage bucket specified for S3 configuration",
        ):
            self.infra(cfg)

        tested_config = deepcopy(self.full_config)

        del tested_config["filestore"]
        with pytest.raises(
            ConfigurationConflict, match="Invalid configuration for S3 found"
        ):
            self.infra(tested_config)

    def test__select_config(self):
        configs = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [
                {"type": "s3", "name": "S3Local"},
                {"type": "s3Legacy", "name": "Swift"},
                {"type": "s3Legacy", "name": "Swift2"},
            ],
        }
        s3 = self.infra(configs)
        conf = s3._select_config("Swift2")
        assert conf == {"type": "s3Legacy", "name": "Swift2"}

        conf = s3._select_config()
        assert conf == {"type": "s3", "name": "S3Local"}

        with pytest.raises(ConfigurationConflict):
            s3._select_config("something else")

    @mock.patch("minty_infra_storage.s3.minio.Minio")
    def test__s3_client(self, mock_minio):
        general_configs = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3", "name": "S3Local"}],
        }
        s3 = self.infra(general_configs)
        filestore_config = {}

        s3._s3_client(filestore_config)

        mock_minio.assert_called_once()

    def test__generate_minio_config(self):
        config = {"addressing_style": "manual", "signature_version": "v3"}
        boto_config = self.s3Client._generate_minio_config(config)
        assert "credentials" in boto_config

    def test__generate_minio_config_hardcoded_key(self):
        config = {
            "access_id": 123,
            "access_key": 456,
            "endpoint_url": "http://localhost:s3/endpoint",
            "region_name": "NL",
        }
        boto_params = self.s3Client._generate_minio_config(config)
        print(boto_params)
        assert boto_params == {
            "access_key": 123,
            "secret_key": 456,
            "secure": False,
            "endpoint": "localhost:s3/endpoint",
            "region": "NL",
        }

    def test__generate_minio_config_hardcoded_key_secure(self):
        config = {
            "access_id": 123,
            "access_key": 456,
            "endpoint_url": "https://localhost:s3/endpoint",
            "region_name": "NL",
        }
        boto_params = self.s3Client._generate_minio_config(config)
        print(boto_params)
        assert boto_params == {
            "access_key": 123,
            "secret_key": 456,
            "secure": True,
            "endpoint": "localhost:s3/endpoint",
            "region": "NL",
        }

    def test__generate_attachment_disposition(self):
        result = self.s3Client._generate_attachment_disposition(
            filename="something", download=True
        )
        assert result == "attachment; filename*=UTF-8''something"

        result2 = self.s3Client._generate_attachment_disposition(
            filename="something", download=False
        )
        assert result2 == "inline; filename*=UTF-8''something"

        result3 = self.s3Client._generate_attachment_disposition(
            filename="an — em dash", download=True
        )
        assert (
            result3
            == "attachment; filename*=UTF-8''an%20%E2%80%94%20em%20dash"
        )
        result4 = self.s3Client._generate_attachment_disposition(
            filename=None, download=True
        )
        assert result4 == "attachment"

    @mock.patch("minty.infrastructure.mime_utils.get_mime_type_from_handle")
    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_s3_upload(self, mock_s3_client, mock_get_mime):
        mock_get_mime.return_value = "fake/mime"

        general_configs = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [
                {"type": "s3", "name": "S3", "bucket": "test_bucket"}
            ],
        }
        s3Client = self.infra(general_configs)

        file_handle = io.BytesIO(b"test")
        mock_s3_client().put_object.return_value = ObjectWriteResult(
            "bucket_name", "object_name", "version_id", '"some_md5"', "dummy"
        )

        file_uuid = uuid4()
        ret_value = s3Client.upload(file_handle, file_uuid)
        mock_get_mime.assert_called()

        mock_s3_client().put_object.return_value = ObjectWriteResult(
            "bucket_name", "object_name", "version_id", '"some_md5"', "dummy"
        )

        mock_s3_client().put_object.assert_called_with(
            data=file_handle,
            length=4,
            object_name=f"test/{file_uuid}",
            bucket_name="test_bucket",
        )

        assert ret_value["uuid"] == file_uuid
        assert ret_value["md5"] == "some_md5"
        assert ret_value["size"] == 4
        assert ret_value["mime_type"] == "fake/mime"
        assert ret_value["storage_location"] == "S3"

    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_s3_upload_exception(self, mock_s3_client):
        general_configs = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3", "name": "S3"}],
        }
        s3Client = self.infra(general_configs)

        file_handle = io.BytesIO(b"test")
        file_uuid = uuid4()

        with pytest.raises(ConfigurationConflict):
            s3Client.upload(file_handle, file_uuid)

    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_get_download_url(self, mock_s3_client):
        s3_wrapper = self.infra(self.full_config)
        uuid = uuid4()
        storage_location = "S3Local"
        filename = "test.png"
        mime_type = "img/png"
        download = True

        mock_s3_client().presigned_get_object.return_value = (
            "https://dev.zaaksysteem.download/from/s3"
        )

        url = s3_wrapper.get_download_url(
            uuid=uuid,
            storage_location=storage_location,
            filename=filename,
            mime_type=mime_type,
            download=download,
        )

        mock_s3_client().presigned_get_object.assert_called_with(
            bucket_name="demo-bucket-002",
            expires=datetime.timedelta(seconds=3600),
            object_name=f"test/{uuid}",
            response_headers={
                "response-content-disposition": "attachment; filename*=UTF-8''test.png",
                "response-cache-control": "private, max-age=3600",
                "response-content-type": "img/png",
            },
        )
        assert url == "https://dev.zaaksysteem.download/from/s3"

    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_get_download_url_exceptions(self, mock_s3_client):
        config = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3", "name": "S3Local"}],
        }

        s3_wrapper = self.infra(config)
        uuid = uuid4()
        storage_location = "S3Local"
        filename = "test.png"
        mime_type = "img/png"
        download = True
        with pytest.raises(ConfigurationConflict):
            s3_wrapper.get_download_url(
                uuid=uuid,
                storage_location=storage_location,
                filename=filename,
                mime_type=mime_type,
                download=download,
            )

        config_no_type = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "nonetype", "name": "S3Local"}],
        }
        s3_wrapper = self.infra(config_no_type)
        with pytest.raises(ConfigurationConflict):
            s3_wrapper.get_download_url(
                uuid=uuid,
                storage_location=storage_location,
                filename=filename,
                mime_type=mime_type,
                download=download,
            )

    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_get_download_url_s3Legacy(self, mock_s3_client):
        config = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3Legacy", "name": "SwiftLocal"}],
        }

        s3_wrapper = self.infra(config)
        uuid = uuid4()
        storage_location = "SwiftLocal"
        filename = "test.png"
        mime_type = "img/png"
        download = True

        mock_s3_client().presigned_get_object.return_value = (
            "https://dev.zaaksysteem.download/from/swift"
        )

        url = s3_wrapper.get_download_url(
            uuid=uuid,
            storage_location=storage_location,
            filename=filename,
            mime_type=mime_type,
            download=download,
        )

        mock_s3_client().presigned_get_object.assert_called_with(
            bucket_name="test",
            expires=datetime.timedelta(seconds=3600),
            object_name=str(uuid),
            response_headers={
                "response-content-disposition": "attachment; filename*=UTF-8''test.png",
                "response-cache-control": "private, max-age=3600",
                "response-content-type": "img/png",
            },
        )
        assert url == "https://dev.zaaksysteem.download/from/swift"

    @mock.patch("minty_infra_storage.s3.S3Wrapper._s3_client")
    def test_download_file(self, mock_s3_client):
        config = {
            "instance_uuid": str(uuid4()),
            "storage_bucket": "test",
            "filestore": [{"type": "s3Legacy", "name": "SwiftLocal"}],
        }

        s3_wrapper = self.infra(config)
        uuid = uuid4()
        storage_location = "SwiftLocal"

        mock_s3_client().generate_presigned_url.return_value = (
            "https://dev.zaaksysteem.download/from/swift"
        )

        get_object_rv = mock.Mock()
        get_object_rv.stream.return_value = [b"data stored in s3"]
        mock_s3_client().get_object.return_value = get_object_rv

        fh = io.BytesIO(initial_bytes=b"")

        rv = s3_wrapper.download_file(
            destination=fh,
            file_uuid=str(uuid),
            storage_location=storage_location,
        )
        assert rv is None

        mock_s3_client().get_object.assert_called_with(
            bucket_name="test", object_name=str(uuid)
        )

        assert fh.getvalue() == b"data stored in s3"

    def test__generate_s3_values_for_type(self):
        config = {"name": "s3", "type": "s3Legacy"}
        uuid = uuid4()
        s3_values = self.s3Client._get_s3_bucket_and_key(
            config=config, uuid=uuid
        )
        s3_values["bucket"] == self.s3Client.base_directory
        s3_values["key"] == str(uuid)

        config = {"name": "s3", "type": "s3", "bucket": "somebucket"}
        uuid = uuid4()
        s3_values = self.s3Client._get_s3_bucket_and_key(
            config=config, uuid=uuid
        )
        s3_values["bucket"] == config["bucket"]
        s3_values["key"] == f"{self.s3Client.base_directory}/{uuid}"

    def test__generate_s3_values_for_exceptions(self):
        config = {"name": "s3", "type": "s3"}
        uuid = uuid4()
        with pytest.raises(ConfigurationConflict):
            self.s3Client._get_s3_bucket_and_key(config=config, uuid=uuid)

        config = {"name": "s3", "type": "no_type"}
        uuid = uuid4()
        with pytest.raises(ConfigurationConflict):
            self.s3Client._get_s3_bucket_and_key(config=config, uuid=uuid)
